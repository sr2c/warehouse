CREATE TABLE IF NOT EXISTS locations (
    id INTEGER PRIMARY KEY,
    type TEXT NOT NULL,
    name TEXT NOT NULL,
    gs1_id TEXT,
    -- TODO: additional location ID
    address_country TEXT CHECK (length(address_country) = 2), -- gs1:addressCountry
    address_country_subdivision TEXT CHECK (length(address_country_subdivision) = 3) -- New field for address_country_subdivision
        DEFAULT NULL, -- Allow the field to be nullable
    address_locality TEXT, -- gs1:addressLocality
    province_state_code TEXT, -- gs1:addressRegion
    address_suburb TEXT, -- gs1:addressSuburb
    county_code TEXT, -- gs1:countyCode
    cross_street TEXT, -- gs1:crossStreet
    organization_name TEXT, -- gs1:organizationName
    po_box_number TEXT, -- gs1:postOfficeBoxNumber
    postal_code TEXT, -- gs1:postalCode
    postal_name TEXT, -- gs1:postalName
    street_address TEXT, -- gs1:streetAddress
    street_address_line2 TEXT, -- gs1:streetAddressLine2
    street_address_line3 TEXT, -- gs1:streetAddressLine3
    street_address_line4 TEXT, -- gs1:streetAddressLine4
    has_certification TEXT, -- gs1:certification
    contact_phone TEXT,
    contact_fax TEXT,
    contact_email TEXT,
    department TEXT, -- gs1:department
    primary_location INTEGER CHECK (type IN ('LEGAL_ENTITY', 'FUNCTION') OR primary_location IS NULL), -- gs1:hasPrimaryLocation (foreign key referencing the id column of the locations table)
    lessee_of_location INTEGER, -- gs1:lesseeOf (foreign key referencing the id column of the locations table)
    managed_by INTEGER, -- gs1:managedBy (foreign key referencing the id column of the locations table)
    occupies_location INTEGER, -- gs1:occupies (foreign key referencing the id column of the locations table)
    owned_by INTEGER, -- gs1:ownedBy (foreign key referencing the id column of the locations table)
    parent_organization INTEGER, -- gs1:parentOrganization (foreign key referencing the id column of the locations table)
    party_gln TEXT CHECK (length(party_gln) = 13), -- gs1:partyGLN
    replaced_organization INTEGER, -- gs1:replacedOrganization (foreign key referencing the id column of the locations table)
    responsible_for_location INTEGER, -- gs1:responsibleForLocation (foreign key referencing the id column of the locations table)
    CONSTRAINT ck_gln_type CHECK (
        type IN ('DIGITAL_LOCATION', 'FIXED_PHYSICAL_LOCATION', 'FUNCTION', 'LEGAL_ENTITY', 'MOBILE_PHYSICAL_LOCATION', 'SUPPLIER', 'CUSTOMER')
    ),
    CONSTRAINT ck_gln_length CHECK (
        gs1_id IS NULL
        OR (type IN ('SUPPLIER', 'CUSTOMER') AND length(gs1_id) = 18)
        OR length(gs1_id) = 13
    ),
        CONSTRAINT ck_address_country CHECK (
        length(address_country) = 2
    ),
    CONSTRAINT ck_address_country_subdivision CHECK (
        length(address_country_subdivision) = 3 OR address_country_subdivision IS NULL -- Allow null values
    ),
    CONSTRAINT fk_primary_location FOREIGN KEY (primary_location) REFERENCES locations (id),
    CONSTRAINT fk_lessee_of_location FOREIGN KEY (lessee_of_location) REFERENCES locations (id),
    CONSTRAINT fk_managed_by FOREIGN KEY (managed_by) REFERENCES locations (id),
    CONSTRAINT fk_occupies_location FOREIGN KEY (occupies_location) REFERENCES locations (id),
    CONSTRAINT fk_owned_by FOREIGN KEY (owned_by) REFERENCES locations (id),
    CONSTRAINT fk_parent_organization FOREIGN KEY (parent_organization) REFERENCES locations (id),
    CONSTRAINT fk_replaced_organization FOREIGN KEY (replaced_organization) REFERENCES locations (id),
    CONSTRAINT fk_responsible_for_location FOREIGN KEY (responsible_for_location) REFERENCES locations (id)
);

CREATE TABLE IF NOT EXISTS documents (
    id INTEGER PRIMARY KEY,
    type TEXT NOT NULL CHECK (type IN ('order', 'invoice', 'delivery_advice', 'delivery_note')),
    date TEXT NOT NULL,
    from_party_id INTEGER,
    to_party_id INTEGER,
    FOREIGN KEY(from_party_id) REFERENCES locations(id),
    FOREIGN KEY(to_party_id) REFERENCES locations(id)
);

CREATE TABLE IF NOT EXISTS trade_items (
    id INTEGER PRIMARY KEY,
    gtin TEXT NOT NULL UNIQUE,
    CHECK (length(gtin) IN (13, 18))
);

CREATE TABLE IF NOT EXISTS document_items (
    id INTEGER PRIMARY KEY,
    document_id INTEGER NOT NULL,
    item_id INTEGER NOT NULL,
    quantity INTEGER NOT NULL,
    price INTEGER,
    FOREIGN KEY(document_id) REFERENCES documents(id),
    FOREIGN KEY(item_id) REFERENCES trade_items(id)
);
