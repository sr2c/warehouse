from textual.containers import Container, Horizontal
from textual.widgets import Label, Button


class ScreenHeading(Label):
    DEFAULT_CSS = """
    ScreenHeading {
        width: 100%;
        height: 1;
        background: $panel;
        content-align: center middle;
    }
    """


class HomeButton(Button):
    DEFAULT_CSS = """
    HomeButton {
        width: 100%;
        content-align: center middle;
        background: lightgrey;
    }
    """


class HomeBox(Container):
    DEFAULT_CSS = """
    HomeBox {
        border: white;
        padding: 1;
    }
    """


class ScalesReadout(Label):
    DEFAULT_CSS = """
    ScalesReadout {
        width: 100%;
        height: 1;
        background: grey;
        content-align: center middle;
    }
    """


class ActionsArea(Horizontal):
    DEFAULT_CSS = """
    ActionsArea {
        height: 3;
    }    
    """
