from typing import Optional

from textual.app import App, ComposeResult
from textual.containers import ScrollableContainer, Horizontal
from textual.css.query import NoMatches
from textual.screen import Screen
from textual.widgets import (
    Button, Label, Header, Footer, Input
)

from app.database import conn
from app.forms.logistics import AddressLabel, ParcelLabel
from app.devices.printing import print_pdf_with_cups
from app.devices.scales import DATA_MODE_GRAMS, Scales
from app.forms.util import format_weight
from app.ui.widgets import ScreenHeading, HomeButton, HomeBox, ScalesReadout, ActionsArea


class AddressLabelScreen(Screen):
    BINDINGS = [
        ("escape", "app.pop_screen()", "Home")
    ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.lines = ["", "", "", "", ""]

    def clear(self):
        for idx in range(1, 6):
            input_field = self.query_one(f"#line{idx}")
            # Use the input changed event to clear the local state too
            input_field.value = ""
        self.query_one("#line1").focus()

    def compose(self) -> ComposeResult:
        yield Header(show_clock=True)
        yield Footer()
        yield ScrollableContainer(
            ScreenHeading("Address Label"),
            Label("Line 1"),
            Input(id="line1"),
            Label("Line 2"),
            Input(id="line2"),
            Label("Line 3"),
            Input(id="line3"),
            Label("Line 4"),
            Input(id="line4"),
            Label("Line 5"),
            Input(id="line5"),
        )
        yield Button("Print", id="print", variant="warning")

    def on_input_changed(self, event: Input.Changed):
        idx = int(event.input.id[-1]) - 1
        self.lines[idx] = event.input.value

    def on_button_pressed(self, event: Button.Pressed):
        if event.button.id == "print":
            self.render_pdf()

    def render_pdf(self):
        label = AddressLabel(self.lines)
        label.render("address.pdf")
        self.clear()
        print_pdf_with_cups("address.pdf", "DYMO_LabelWriter_450")
        app.pop_screen()


class ParcelLabelScreen(Screen):
    BINDINGS = [
        ("escape", "app.pop_screen()", "Home")
    ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.sender = ["SR2 Communications Limited", "499 Union Street", "Aberdeen", "AB11 6DB", "GB-SCT"]
        self.sender_gln = ""
        self.sender_phone = "+441224900202"
        self.recipient = ["", "", "", "", ""]
        self.recipient_gln = ""
        self.recipient_phone = "+44123"

    def clear(self):
        for idx in range(1, 6):
            input_field = self.query_one(f"#line{idx}")
            # Use the input changed event to clear local state too
            input_field.value = ""
        self.query_one("#line1").focus()

    def compose(self) -> ComposeResult:
        yield Header(show_clock=True)
        yield ScalesReadout(id="scales")
        yield ScrollableContainer(
            ScreenHeading("Parcel Label"),
            Label("Sender will be SR2 Communications Limited from 499 Union Street.\n"),
            Label("Recipient Name"),
            Input(id="line1"),
            Label("Recipient Street Address"),
            Input(id="line2"),
            Label("Recipient City"),
            Input(id="line3"),
            Label("Recipient Postcode"),
            Input(id="line4"),
            Label("Recipient Country (ISO Alpha-2 or ISO Alpha-2 + Alpha-3 subdivision)"),
            Input(id="line5"),
            Label("Recipient Telephone"),
            Input(id="phone"),
            Label("Contents (GTIN)"),
            Input(id="contents"),
            Label("Quantity"),
            Input(id="quantity"),
        )
        yield ActionsArea(
            Button("Print", id="print", variant="warning"),
            Button("Clear", id="clear", variant="error"),
        )
        yield Footer()

    def on_input_changed(self, event: Input.Changed):
        if event.input.id == "phone":
            self.recipient_phone = event.input.value
        else:
            idx = int(event.input.id[-1]) - 1
            self.recipient[idx] = event.input.value

    def on_button_pressed(self, event: Button.Pressed):
        if event.button.id == "print":
            self.render_pdf()
        if event.button.id == "clear":
            self.clear()

    def render_pdf(self):
        ParcelLabel(
            sscc=None,
            sender_address=self.sender,
            sender_gln=self.sender_gln,
            sender_phone=self.sender_phone,
            recipient_address=self.recipient,
            recipient_gln=self.recipient_gln,
            recipient_phone=self.recipient_phone,
            gross_weight=app.grams
        ).render("parcel.pdf")
        self.clear()
        print_pdf_with_cups("parcel.pdf", "DYMO_DYMO_LabelWriter_5XL")
        app.pop_screen()


def update_scales() -> None:
    result = Scales().get_weight()
    app.grams = result
    try:
        scales_readout = app.query_one("#scales")
        if app.grams is not None:
            scales_readout.update("Current Weight: " + format_weight(app.grams))
            scales_readout.add_class("activeScales")
        else:
            scales_readout.update("Current Weight Unavailable")
            scales_readout.remove_class("activeScales")
    except NoMatches:
        pass


class ReceptionDesk(App):
    TITLE = "SR2 Communications Limited"
    SCREENS = {
        "address": AddressLabelScreen(),
        "parcel": ParcelLabelScreen(),
    }
    CSS = """
    .activeScales {
        background: purple;
    }
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.grams: Optional[int] = None

    def compose(self) -> ComposeResult:
        """Create child widgets for the app."""
        app.set_interval(1, update_scales)
        yield Header(show_clock=True)
        yield ScalesReadout("Current Weight Initialising", id="scales")
        yield Horizontal(
            HomeBox(
                ScreenHeading("Actions"),
                HomeButton("Address Label", id="address"),
                HomeButton("Parcel Label", id="parcel")
            ),
            HomeBox(
                ScreenHeading("Information")
            ))
        yield Footer()

    def on_button_pressed(self, event: Button.Pressed) -> None:
        if event.button.id == "address":
            app.push_screen("address")
        elif event.button.id == "parcel":
            app.push_screen("parcel")

    async def exit_program(self, button):
        await self.quit()

    async def on_stop(self, event):
        conn.close()


if __name__ == "__main__":
    cursor = conn.cursor()
    app = ReceptionDesk()
    app.run()
