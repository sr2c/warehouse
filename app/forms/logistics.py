"""
Logistics Labels
"""

from typing import List, Optional

from reportlab.lib.units import mm

from app.forms.barcode import Gs1OneTwoEight, Sscc18
from app.forms.util import output_canvas, icon_path, horizontal_line, format_weight, LARGE_ADDRESS_LABEL, SHIPPING_LABEL


class AddressLabel:
    lines: List[str]

    def __init__(self, lines: List[str]):
        if len(lines) > 5:
            raise NotImplementedError("This implementation can only produce labels with up to 5 lines of text.")
        self.lines = lines

    def render(self, filename: str):
        c = output_canvas(filename, pagesize=LARGE_ADDRESS_LABEL)
        c.setFont("Helvetica", 13)
        for idx, line in enumerate(self.lines):
            c.drawString(15, LARGE_ADDRESS_LABEL[1] - (17 * (idx + 1)), line)
        c.save()


class ParcelLabel:
    sender_address: List[str]
    sender_phone: str
    sender_gln: str
    recipient_address: List[str]
    recipient_phone: str
    recipient_gln: str
    gross_weight: Optional[int]

    def __init__(self, *,
                 sscc: Optional[str],
                 sender_address: List[str],
                 sender_phone: Optional[str],
                 sender_gln: Optional[str],
                 recipient_address: List[str],
                 recipient_phone: str,
                 recipient_gln: str,
                 gross_weight: Optional[int]):
        self.sscc = sscc
        self.sender_address = sender_address
        self.sender_phone = sender_phone
        self.sender_gln = sender_gln
        self.recipient_address = recipient_address
        self.recipient_phone = recipient_phone
        self.recipient_gln = recipient_gln
        self.gross_weight = gross_weight

    def render(self, filename: str) -> None:
        if self.sscc:
            barcode_sscc = Sscc18(f"(00){self.sscc}").image_reader()
        else:
            barcode_sscc = None
        ship_to_post = Gs1OneTwoEight(f'(420){self.recipient_address[3].replace(" ", "")}').image_reader()
        c = output_canvas(filename, pagesize=SHIPPING_LABEL)
        c.drawImage(icon_path("logo"), 10, SHIPPING_LABEL[1] - (23 * mm), 20 * mm, 20 * mm, preserveAspectRatio=True)
        if self.gross_weight is not None:
            c.setFont("Helvetica", 8)
        if self.gross_weight is not None:
            c.drawString(52.5 * mm, SHIPPING_LABEL[1] - 30, f"Gross weight: {format_weight(self.gross_weight)}")
        horizontal_line(c, 25)
        # From
        c.saveState()
        c.translate(20, SHIPPING_LABEL[1] - (45 * mm))
        c.rotate(90)
        c.setFont("Helvetica-Bold", 11)
        c.drawString(0, 0, "FROM")
        c.restoreState()
        c.setFont("Helvetica", 9)
        for idx, line in enumerate(self.sender_address):
            c.drawString(30, SHIPPING_LABEL[1] - ((27 * mm) + (10 * (idx + 1))), line)
        c.setFont("Helvetica", 8)
        if self.sender_phone:
            c.drawImage(icon_path("telephone"), SHIPPING_LABEL[0] - (36 * mm), SHIPPING_LABEL[1] - ((30 * mm) + 10),
                        6 * mm,
                        6 * mm)
            c.drawString(SHIPPING_LABEL[0] - (30 * mm), SHIPPING_LABEL[1] - (31.5 * mm), self.sender_phone)
        horizontal_line(c, 55)
        # To
        c.setFont("Helvetica-Bold", 16)
        c.drawString(10, SHIPPING_LABEL[1] - (65 * mm), "TO")
        c.setFont("Helvetica", 11)
        for idx, line in enumerate(self.recipient_address):
            c.drawString(40, SHIPPING_LABEL[1] - ((57 * mm) + (14 * (idx + 1))), line)
        c.setFont("Helvetica", 8)
        if self.recipient_phone:
            c.drawImage(icon_path("telephone"), SHIPPING_LABEL[0] - (36 * mm), SHIPPING_LABEL[1] - ((60 * mm) + 10),
                        6 * mm, 6 * mm)
            c.drawString(SHIPPING_LABEL[0] - (30 * mm), SHIPPING_LABEL[1] - (60.5 * mm), self.recipient_phone)
        horizontal_line(c, 90)
        horizontal_line(c, 105)
        c.setFont("Helvetica-Bold", 7)
        c.drawCentredString(SHIPPING_LABEL[0] / 2, SHIPPING_LABEL[1] - (108 * mm), "SHIP TO POST")
        c.drawImage(ship_to_post, (5 * mm), SHIPPING_LABEL[1] - (130.5 * mm), SHIPPING_LABEL[0] - (10 * mm),
                    21.5 * mm)
        if self.sscc:
            c.drawCentredString(SHIPPING_LABEL[0] / 2, SHIPPING_LABEL[1] - (133 * mm), "SSCC")
            c.drawImage(barcode_sscc, (5 * mm), 10, SHIPPING_LABEL[0] - (10 * mm),
                        21.5 * mm)
        c.save()


if __name__ == "__main__":
    AddressLabel([f"Line {idx}" for idx in range(1, 6)]).render("address.pdf")
    ParcelLabel(
        sscc="350609791900000313",
        sender_address=[
            "SR2 Communications Limited",
            "499 Union Street",
            "Aberdeen",
            "AB11 6DB",
            "GB-SCT"
        ],
        sender_phone="+441224900202",
        sender_gln="5060979190022",
        recipient_address=[
            "SR2 Communications Limited",
            "4-5 James Gregory Centre",
            "Aberdeen",
            "AB22 8GU",
            "GB-SCT"
        ],
        recipient_phone="+441224900280",
        recipient_gln="5060979190015",
        gross_weight=1329
    ).render("parcel.pdf")
