import io

import treepoem
from reportlab.lib.utils import ImageReader


class Gs1OneTwoEight(io.BytesIO):
    def __init__(self, barcode_data: str):
        super().__init__()
        image = treepoem.generate_barcode(
            barcode_type="gs1-128",
            data=barcode_data,
            options={"includetext": True, "textfont": "Helvetica", "textsize": 6}
        )
        image.save(self, format="PNG")
        self.seek(0)

    def image_reader(self) -> ImageReader:
        return ImageReader(self)

    def export(self, filename: str):
        # TODO: May need to add iteration here to do this properly, but the files are probably small.
        with open(filename, 'wb') as output:
            output.write(self.read())


class Sscc18(io.BytesIO):
    def __init__(self, barcode_data: str):
        super().__init__()
        image = treepoem.generate_barcode(
            barcode_type="sscc18",
            data=barcode_data,
            options={"includetext": True, "textfont": "Helvetica", "textsize": 6}
        )
        image.save(self, format="PNG")
        self.seek(0)

    def image_reader(self) -> ImageReader:
        return ImageReader(self)

    def export(self, filename: str):
        # TODO: May need to add iteration here to do this properly, but the files are probably small.
        with open(filename, 'wb') as output:
            output.write(self.read())


if __name__ == "__main__":
    Gs1OneTwoEight("(420)AB116DB").export("ship_to_post.png")
