import os.path
from typing import Tuple

from reportlab.lib.units import mm
from reportlab.pdfgen import canvas
from reportlab.pdfgen.canvas import Canvas


def icon_path(icon_name: str) -> str:
    return os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'assets', f"{icon_name}.png"))


def format_weight(grams: int) -> str:
    if grams < 500:
        return f"{grams} g"  # Format as grams
    else:
        return f"{grams / 1000:.1f} kg"  # Format as kilograms


def horizontal_line(c: canvas.Canvas, height: float):
    c.line(0, SHIPPING_LABEL[1] - (height * mm), SHIPPING_LABEL[0], SHIPPING_LABEL[1] - (height * mm))


def output_canvas(filename: str, pagesize: Tuple[float, float]):
    return Canvas(output_filename(filename), pagesize=pagesize)


def output_filename(filename: str):
    return os.path.join(os.path.dirname(__file__), '..', '..', 'output', filename)


LARGE_ADDRESS_LABEL = (88.9 * mm, 35.63 * mm)
SHIPPING_LABEL = (104.39 * mm, 159.43 * mm)
