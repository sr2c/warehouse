import subprocess

from app.forms.util import output_filename


def print_pdf_with_cups(pdf_file, printer_name=None):
    # Construct the lp command with the PDF file path and printer name
    command = ['lp']
    if printer_name:
        command.extend(['-d', printer_name])
    command.append(output_filename(pdf_file))

    # Execute the lp command using subprocess
    try:
        subprocess.run(command, check=True)
        return True
    except subprocess.CalledProcessError as e:
        return False
