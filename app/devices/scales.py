import math
from typing import Optional

import usb.core

DATA_MODE_GRAMS = 2
DATA_MODE_OUNCES = 11

VENDOR_ID = 0x0922
PRODUCT_ID = 0x8003


class Scales(object):

    def __init__(self, vendor_id=VENDOR_ID, product_id=PRODUCT_ID):
        self.vendor_id = vendor_id
        self.product_id = product_id
        self.device = None
        self._connect()

    def _connect(self):
        self.device = usb.core.find(idVendor=self.vendor_id,
                                    idProduct=self.product_id)

        if self.device is None:
            return

        if self.device.is_kernel_driver_active(0):
            self.device.detach_kernel_driver(0)

        self.device.set_configuration()
        self.endpoint = self.device[0][(0, 0)][0]

    def __del__(self):
        if self.device is not None:
            self.device.reset()

    def get_weight(self) -> Optional[int]:
        if self.device is None:
            return None
        attempts = 1  # TODO: Refactor this to cope with retries but not all in this function, treat it more as a "tick"
        data = None
        while data is None and attempts > 0:
            try:
                data = self.device.read(self.endpoint.bEndpointAddress,
                                        self.endpoint.wMaxPacketSize)
            except usb.core.USBError as e:
                data = None
                if e.args == ('Operation timed out',):
                    attempts -= 1
                    continue

        weight = None
        raw_weight = data[4] + data[5] * 256

        if data[2] == DATA_MODE_OUNCES:
            scaling_factor = math.pow(10, (data[3] - 256))
            ounces = raw_weight * scaling_factor
            weight = ounces * 28.3495  # Convert to grams
        elif data[2] == DATA_MODE_GRAMS:
            grams = raw_weight
            weight = grams

        if data[1] == 5:
            weight = weight * -1

        return weight
